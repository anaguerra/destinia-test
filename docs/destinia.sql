-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.21 - MySQL Community Server (GPL)
-- SO del servidor:              Linux
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para destinia
CREATE DATABASE IF NOT EXISTS `destinia` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `destinia`;

-- Volcando estructura para tabla destinia.accommodation
CREATE TABLE IF NOT EXISTS `accommodation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Hotel','Apartment') NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '0',
  `province` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla destinia.accommodation: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `accommodation` DISABLE KEYS */;
INSERT INTO `accommodation` (`id`, `type`, `name`, `city`, `province`) VALUES
	(1, 'Hotel', 'Hotel Azul', 'Valencia', 'Valencia'),
	(2, 'Hotel', 'Hotel Blanco', 'Mojacar', 'Almería'),
	(3, 'Hotel', 'Hotel Rojo', 'Sanlucar', 'Cádiz'),
	(4, 'Apartment', 'Apartamentos Beach', 'Almería', 'Almería'),
	(5, 'Apartment', 'Apartamentos Sol y playa', 'Málaga', 'Málaga'),
	(6, 'Hotel', 'Hotel Beach Canteras', 'Las Palmas', 'Las Palmas');
/*!40000 ALTER TABLE `accommodation` ENABLE KEYS */;

-- Volcando estructura para tabla destinia.apartment
CREATE TABLE IF NOT EXISTS `apartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accommodation_id` int(11) NOT NULL DEFAULT '0',
  `qty` int(11) NOT NULL DEFAULT '0',
  `adults` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_apartment_accomodation` (`accommodation_id`),
  CONSTRAINT `fk_apartment_accomodation` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla destinia.apartment: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `apartment` DISABLE KEYS */;
INSERT INTO `apartment` (`id`, `accommodation_id`, `qty`, `adults`) VALUES
	(1, 4, 10, 4),
	(2, 5, 50, 6);
/*!40000 ALTER TABLE `apartment` ENABLE KEYS */;

-- Volcando estructura para tabla destinia.hotel
CREATE TABLE IF NOT EXISTS `hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accommodation_id` int(11) NOT NULL DEFAULT '0',
  `stars` tinyint(4) NOT NULL,
  `room_standard` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_accomodation` (`accommodation_id`),
  CONSTRAINT `fk_hotel_accomodation` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla destinia.hotel: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` (`id`, `accommodation_id`, `stars`, `room_standard`) VALUES
	(1, 1, 3, 'Habitación doble con vistas'),
	(2, 2, 4, 'Habitación doble'),
	(3, 3, 3, 'Habitación sencilla'),
	(4, 6, 5, 'Habitación doble');
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
