<?php

require_once 'bootstrap.php';

use Destinia\Classes\Connection;
use Destinia\Classes\Base;

$connection = new Connection();

Base::init($connection)->run();

