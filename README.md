Destinia test
--------------


Seguir estos pasos:

1) `composer install`

2) Cambiar los datos de host de db en el fichero config/database.php

3) Importar la base de datos de docs/destinia.sql

4) Ejecutar por línea de comandos `php index.php`

Hay dos opciones de búsqueda, por provincia y por nombre de alojamiento. No se si es exactamente lo que querían, es difícil
sin poder hacer preguntas.

Cualquier duda me dicen.


