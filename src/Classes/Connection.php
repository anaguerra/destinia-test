<?php

namespace Destinia\Classes;

use \PDO;
use \PDOException;
use \PDOStatement;

/**
 * Class Connection
 * @package Destinia\Classes
 */
class Connection extends PDO
{
    /**
     * @var PDO
     */
    private $dbh;


    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;



    /**
     * @var string
     */
    private $error;

    /**
     * @var PDOStatement
     */
    private $stmt;

    /**
     * @var string
     */
    private $dsn;

    /**
     * @var array
     */
    private $options;

    /**
     * @var
     */
    private $port;


    public function __construct()
    {
        $dbData = $this->getConfig();

        $this->setConnectionData($dbData);
        $this->getConnection();
        parent::__construct($this->dsn, $this->user, $this->pass, $this->options);
    }



    public function __destruct()
    {
        $this->close_con();
    }



    /**
     * Get connection database
     */
    private function getConnection()
    {
        // Create new PDO instance
        try {
            $this->dbh = new PDO($this->dsn, $this->user, $this->pass, $this->options);

        } catch(PDOException $e) {
            $this->error = $e->getMessage();
        }
    }



    private function setConnectionData($dbData)
    {
        $this->dsn = 'mysql:host='. $dbData['host']. ';dbname='. $dbData['dbName']. ';';

        $this->port = $dbData['dbPort'];
        $this->user = $dbData['dbUser'];
        $this->pass = $dbData['dbPassword'];
        $this->options = array(
            PDO::ATTR_PERSISTENT    => true,
            PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        );
    }


    /**
     * @param string $query
     * @return \PDOStatement|void
     */
    public function query($query)
    {
        try {
            $this->stmt = $this->dbh->prepare($query);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }
    }



    /**
     * @param $param
     * @param $value
     * @param null $type
     */
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
                    break;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        return $this->stmt->execute();
    }

    /**
     * @return mixed
     */
    public function resultset()
    {
        $this->execute();
        return $this->stmt->fetchColumn();
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @param null $seqname
     * @return string
     */
    public function lastInsertId($seqname = null)
    {
        return $this->dbh->lastInsertId($seqname);
    }

    /**
     * Close connection
     */
    public function close_con()
    {
        $this->dbh = null;
    }



    /**
     * Load config database host file
     * @throws \Exception
     */

    public function getConfig()
    {
        $file = ROOTPATH . DS  .'config/database.php';

        if (!file_exists($file)) {
            throw new \Exception('Cannot find database config file "' . $file);
        }

        $dbHosts = require($file);
        return $dbHosts;
    }

}