<?php
namespace Destinia\Classes;

use Destinia\Entity\Accommodation;
use Destinia\Repository\AccommodationRepository;

/**
 * Class Base
 */
class Base
{
    private $line;
    private $connection;


    /**
     * Base constructor.
     * @param Connection $connection
     */
    private function __construct(Connection $connection)
    {
        $this->accommodationRepository = new AccommodationRepository($connection);
    }


    /**
     * @param Connection $connection
     * @return Base
     */
	public static function init(Connection $connection)
	{
		return new self($connection);
    }



    public function run()
    {
        echo <<<EOF
**** DESTINIA TEST ***** \n
Para búsqueda por nombre del alojamiento, teclee 'N' y Enter
Para búsqueda por provincia del alojamiento, teclee 'P' y Enter\n
:
EOF;

        $this->line = $this->getStdinLine();

        switch (trim($this->line)) {
            case 'N':
                $this->searchByName();
                break;
            case 'P':
                $this->searchByProvince();
                break;
            default:
                die('fin');
                $this->setStdinError();
                break;
        }
    }


    private function searchByName()
    {
        echo <<<EOF
Introduzca palabra de búsqueda 
:
EOF;
        $line = $this->getStdinLine();
        $search = substr($line, 0, 3);

        $accommodations = $this->accommodationRepository->findByName($search);

        $this->printOutput($accommodations);
    }



    private function searchByProvince()
    {
        echo <<<EOF
Introduzca provincia de búsqueda 
:
EOF;
        $line = $this->getStdinLine();
        $search = substr($line, 0, 3);

        $accommodations = $this->accommodationRepository->findByProvince($search);

        $this->printOutput($accommodations);
    }




    private function getStdinLine()
    {
        $handle = fopen("php://stdin","r");
        $line = fgets($handle);
        fclose($handle);
        return $line;
    }



    private function printOutput($accommodations)
    {
        if (count($accommodations) == 0) {
            echo "No se encontraon resultados\n";
            exit;
        }

        foreach ($accommodations as $acc) {
            echo $acc . "\n";
        }
    }

}