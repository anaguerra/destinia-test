<?php

namespace Destinia\Entity;


class Apartment extends Accommodation
{

    private $id;

    private $qty;

    private $adults;


    /**
     * Hotel constructor.
     * @param $name
     * @param $city
     * @param $province
     * @param $qty
     * @param $adults
     */
    public function __construct($name, $city, $province, $qty, $adults)
    {
        parent::__construct($name, $city, $province);
        $this->qty = $qty;
        $this->adults = $adults;

    }


    public function __toString()
    {
        return "$this->name, $this->qty apartamentos, $this->adults adultos, $this->city, $this->province";
    }

}




