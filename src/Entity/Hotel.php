<?php

namespace Destinia\Entity;


class Hotel extends Accommodation
{

    private $id;

    private $stars;

    private $room;


    /**
     * Hotel constructor.
     * @param $name
     * @param $city
     * @param $province
     * @param $stars
     * @param $room
     */
    public function __construct($name, $city, $province, $stars, $room)
    {
        parent::__construct($name, $city, $province);
        $this->stars = $stars;
        $this->room = $room;

    }


    public function __toString()
    {
        return "$this->name, $this->stars estrellas, $this->room, $this->city, $this->province";
    }

}




