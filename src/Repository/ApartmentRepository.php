<?php


namespace Destinia\Repository;


use Destinia\Classes\Connection;
use \PDO;

class ApartmentRepository
{
    /**
     * @var Connection
     */
    private $connection;



    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }



    /**
     * @param $accommodationId
     * @return mixed
     */
    public function findByAccommodationId($accommodationId)
    {
        $sql = 'SELECT * from apartment WHERE accommodation_id = :accommodation_id';

        $this->connection->query($sql);
        $this->connection->bind(':accommodation_id', $accommodationId, PDO::PARAM_INT);

        $result = $this->connection->getAll();
        return $result[0];
    }


}