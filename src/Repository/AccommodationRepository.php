<?php


namespace Destinia\Repository;


use Destinia\Classes\Connection;
use Destinia\Entity\Apartment;
use Destinia\Entity\Hotel;
use \PDO;

class AccommodationRepository
{
    const TYPE_HOTEL = 'Hotel';
    const TYPE_APARTMENT = 'Apartment';


    /**
     * @var Connection
     */
    private $connection;

    private $hotelRepository;
    private $apartmentRepository;


    /**
     * AccommodationRepository constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->hotelRepository = new HotelRepository($connection);
        $this->apartmentRepository = new ApartmentRepository($connection);
    }


    /**
     * @param $name
     * @return array
     */
    public function findByName($name)
    {
        $name = strtolower($name);
        $sql = 'SELECT * from accommodation WHERE name LIKE :name';

        $this->connection->query($sql);
        $this->connection->bind(':name', "%$name%", PDO::PARAM_STR);

        $result = $this->connection->getAll();
        return $this->getAllDataAccomodation($result);
    }


    /**
     * @param $province
     * @return array
     */
    public function findByProvince($province)
    {
        $province = strtolower($province);
        $sql = 'SELECT * from accommodation WHERE province LIKE :province';

        $this->connection->query($sql);
        $this->connection->bind(':province', "%$province%", PDO::PARAM_STR);

        $result = $this->connection->getAll();
        return $this->getAllDataAccomodation($result);
    }




    /**
     * Array of accomodations
     * @param $result
     * @return array
     */
    private function getAllDataAccomodation($result)
    {
        $accommodations = [];

        foreach ($result as $r) {

            if ($r['type'] == self::TYPE_HOTEL) {
                $accommodations[] = $this->getHotelFromAccommodationData($r);
            } else {
                $accommodations[] = $this->getApartmentFromAccommodationData($r);
            }
        }

        return $accommodations;
    }


    /**
     * @param $accData
     * @return mixed
     */
    private function getApartmentFromAccommodationData($accData)
    {
        $apartmentData = $this->apartmentRepository->findByAccommodationId($accData['id']);
        $apartment = new Apartment($accData['name'], $accData['city'], $accData['province'], $apartmentData['qty'], $apartmentData['adults']);
        return $apartment->__toString();
    }




    /**
     * @param $accData
     * @return mixed
     */
    private function getHotelFromAccommodationData($accData)
    {
        $hotelData = $this->hotelRepository->findByAccommodationId($accData['id']);
        $hotel = new Hotel($accData['name'], $accData['city'], $accData['province'], $hotelData['stars'], $hotelData['room_standard']);
        return $hotel->__toString();
    }


}